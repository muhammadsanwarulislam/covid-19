<footer class="footer">
    <div class="container">
        <nav>
            <p style="text-align:justify;font-size: 13px;text-justify: inter-word;">
                This COVID-19 Self Assessment System is only for software
                development purpose and may not yield actual result. Any
                information given by users of this system will not be disclosed
                or store to anywhere.
            </p>
        </nav>
    </div>
</footer>
