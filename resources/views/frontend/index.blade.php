@extends('frontend.layouts.app')

@section('title')
{{app_name()}}
@endsection

@section('content')

<div class="page-header">
    <div class="page-header-image" data-parallax="true" style="background-image: url('{{asset('img/cover-01.jpg')}}');">
    </div>
    <div class="content-center">
        <div class="container">
            <h1 class="title text-center">
                {{ config('app.name') }}
            </h1>

            <a href="{{ route('frontend.step.one') }}" rel="tooltip" title="আসুন আপনার প্রতিরোধ ক্ষমতা পরীক্ষা করার জন্য খেলি" type="submit" class="btn btn-primary btn-round btn-block">Let's Start</a>

            @include('flash::message')
            @if (session('status'))
            <p class="alert alert-success">{{ session('status') }}</p>
            @endif

        </div>
    </div>
</div>

@endsection