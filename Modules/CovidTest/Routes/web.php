<?php
/*
*
* Frontend Routes
*
* --------------------------------------------------------------------
*/
Route::get('process_initial', 'Frontend\CovidTestFirstController@index')
														->name('frontend.index');

Route::get('process_one', 'Frontend\CovidTestFirstController@createStepOne')
														->name('frontend.step.one'); 

Route::post('process_two', 'Frontend\CovidTestFirstController@postCreateStepOne')
														->name('frontend.step.two'); 

Route::get('process_three', 'Frontend\CovidTestFirstController@createStepTwo')
														->name('frontend.step.three');

Route::post('process_four', 'Frontend\CovidTestFirstController@postCreateStepTwo')
														->name('frontend.step.four');

Route::get('process_five', 'Frontend\CovidTestFirstController@createStepThree')
                                                        ->name('frontend.step.five');

Route::post('process_six', 'Frontend\CovidTestFirstController@postCreateStepThree')
                                                        ->name('frontend.step.six');

/*
*
* Backend Routes
*
* --------------------------------------------------------------------
*/
Route::group(['namespace' => '\Modules\CovidTest\Http\Controllers\Backend', 'as' => 'backend.', 'middleware' => ['web', 'auth', 'can:view_backend'], 'prefix' => 'admin'], function () {

    $module_name = 'thirdtest';
    $controller_name = 'CovidTestThirdController';
    Route::resource("$module_name", "$controller_name");

    $module_name = 'secondtest';
    $controller_name = 'CovidTestSecondController';
    Route::resource("$module_name", "$controller_name");

    $module_name = 'report';
    $controller_name = 'CovidReportController';
    Route::resource("$module_name", "$controller_name");

});