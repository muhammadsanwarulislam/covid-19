<?php

namespace Modules\CovidTest\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /*
         *
         * Module Menu for Admin Backend
         *
         * *********************************************************************
         */
        \Menu::make('admin_sidebar', function ($menu) {

            // Separator: Module Management
            $all_modules = $menu->add('Modules', [
                'class' => 'c-sidebar-nav-title',
            ])
            ->data('order', 100);

            // Covid test Dropdown
            $articles_menu = $menu->add('<i class="c-sidebar-nav-icon fas fa-file-alt"></i>Covid Test', [
                'class' => 'c-sidebar-nav-dropdown',
            ])
            ->data([
                'order'         => 101,
                'activematches' => [
                    'admin/posts*',
                    'admin/categories*',
                ],
                'permission' => ['view_posts', 'view_categories'],
            ]);
            $articles_menu->link->attr([
                'class' => 'c-sidebar-nav-dropdown-toggle',
                'href'  => '#',
            ]);

            // Submenu: CovidTest
            $articles_menu->add('<i class="c-sidebar-nav-icon fas fa-file-alt"></i> Second Test', [
                'route' => 'backend.secondtest.index',
                'class' => 'c-sidebar-nav-item',
            ])
            ->data([
                'order'         => 102,
                'activematches' => 'admin/posts*',
                'permission'    => ['edit_posts'],
            ])
            ->link->attr([
                'class' => "c-sidebar-nav-link",
            ]);

            $articles_menu->add('<i class="c-sidebar-nav-icon fas fa-file-alt"></i> Third Test', [
                'route' => 'backend.thirdtest.index',
                'class' => 'c-sidebar-nav-item',
            ])
            ->data([
                'order'         => 103,
                'activematches' => 'admin/posts*',
                'permission'    => ['edit_posts'],
            ])
            ->link->attr([
                'class' => "c-sidebar-nav-link",
            ]);
            $articles_menu->add('<i class="c-sidebar-nav-icon fas fa-file-alt"></i> Report', [
                'route' => 'backend.report.index',
                'class' => 'c-sidebar-nav-item',
            ])
            ->data([
                'order'         => 104,
                'activematches' => 'admin/posts*',
                'permission'    => ['edit_posts'],
            ])
            ->link->attr([
                'class' => "c-sidebar-nav-link",
            ]);
        })->sortBy('order');

        return $next($request);
    }
}
