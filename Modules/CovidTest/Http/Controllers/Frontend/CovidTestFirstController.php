<?php

namespace Modules\CovidTest\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CovidTest\Entities\FirstTest;
use Modules\CovidTest\Entities\SecondTest;
use Modules\CovidTest\Entities\ThirdTest;
use Flash;

class CovidTestFirstController extends Controller
{
    public function __construct()
    {
        // Page Title
        $this->module_title = 'CovidTest';

        // module name
        $this->module_name  = 'firsttest';

        // directory path of the module
        $this->module_path  = 'firsttest';

        // module model name, path
        $this->module_model = "Modules\CovidTest\Entities\FirstTest";
    }

    public function index()
    {
        $module_model   = $this->module_model;

        $list_of_test   = $module_model::orderby('id','desc')->get();
        // dd($list_of_test);
        return view("covidtest::frontend.index",compact('list_of_test'));
    }

    public function createStepOne(Request $request)
    {
        $covidtest = $request->session()->get('covidtest');
        return view('covidtest::frontend.create-step-one',compact('covidtest'));
    }

    public function postCreateStepOne(Request $request)
    {
        $module_model   = $this->module_model;

        $validatedData  = $request->validate([
            'age'       => 'required',
            'sex'       => 'required',
            'body_temp' => 'required',
        ]);

        if(empty($request->session()->get('covidtest'))){
            $covidtest = new $module_model();
            $covidtest->fill($validatedData);
            $request->session()->put('covidtest', $covidtest);
        }else{
            $covidtest = $request->session()->get('covidtest');
            $covidtest->fill($validatedData);
            $request->session()->put('covidtest', $covidtest);
        }

        return redirect()->route('frontend.step.three');
    }

    public function createStepTwo(Request $request)
    {
        $covidtest = $request->session()->get('covidtest');
        $secondtest =SecondTest::all();
        return view('covidtest::frontend.create-step-two',compact('covidtest','secondtest'));
    }


    public function postCreateStepTwo(Request $request)
    {
        $validatedData = $request->validate([
            'secondtest_item'  => '',
        ]);

        $covidtest = $request->session()->get('covidtest');
        $covidtest->fill($validatedData);
        $request->session()->put('covidtest', $covidtest);

        return redirect()->route('frontend.step.five');
    }

    public function createStepThree(Request $request)
    {
        $covidtest = $request->session()->get('covidtest');
        $thirdtest = ThirdTest::all();
        return view('covidtest::frontend.create-step-three',compact('covidtest','thirdtest'));
    }
    public function postCreateStepThree(Request $request)
    {
        $validatedData = $request->validate([
            'thirdtest_item'  => '',
        ]);
        $covidtest = $request->session()->get('covidtest');
        $covidtest->fill($validatedData);
        $request->session()->put('covidtest', $covidtest);

        $secondtest     = SecondTest::all();
        $thirdtest      = ThirdTest::all();

        //To count second and third test score dynamically from database
        $second_test_score  = $secondtest[0]->score;
        $third_test_score   = $thirdtest[0]->score;

        //Count total select item from the radion button
        if(!empty($covidtest->secondtest_item)){
            $second_symptoms     = count($covidtest->secondtest_item);
        }else{
            $second_symptoms = $covidtest->secondtest_item;
        }
        if(!empty($covidtest->thirdtest_item)){
            $third_symptoms     = count($covidtest->thirdtest_item);
        }else{
            $third_symptoms = $covidtest->thirdtest_item;
        }
        
        
        //Convert the selected ARRAY item will be jason format
        $covidtest->secondtest_item = json_encode($covidtest->secondtest_item);
        $covidtest->thirdtest_item  = json_encode($covidtest->thirdtest_item);


        //Check first condition where human body temperature will socre 2 if it is grater than 37.5 
        if($covidtest->body_temp > 37.5){
            $covidtest->firsttest_score = '2';
 
        }else{
            $covidtest->firsttest_score = null;
        }

        //Check second condition where first item will score 3 and next item will be 1 like(3+1+1)
        if($second_symptoms > 1){

            $count = $second_symptoms + $second_test_score - 1;
            $covidtest->secondtest_score = $count;
        }elseif(!empty($second_symptoms)){
            $covidtest->secondtest_score = $second_test_score;
            
        }else{
            $covidtest->secondtest_score = null;
        }

        //Check third condition where first item will score 2 and next item will be 2 like(2+2+2)
        if($third_symptoms > 1){

            $count = $third_symptoms * $third_test_score;
            $covidtest->thirdtest_score = $count;
        }elseif(!empty($third_symptoms)){
            $covidtest->thirdtest_score = $third_test_score;
            
        }else{
            $covidtest->thirdtest_score = null;
        }

        $covidtest->save();
        $request->session()->forget('covidtest');
        return redirect()->route('frontend.index');
    }
}
