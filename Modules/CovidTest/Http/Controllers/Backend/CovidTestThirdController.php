<?php

namespace Modules\CovidTest\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;
use Flash;

class CovidTestThirdController extends Controller
{
    public function __construct()
    {
        // Page Title
        $this->module_title = 'CovidTest';

        // module name
        $this->module_name = 'thirdtest';

        // directory path of the module
        $this->module_path = 'thirdtest';

        // module icon
        $this->module_icon = 'fas fa-file-alt';

        // module model name, path
        $this->module_model = "Modules\CovidTest\Entities\ThirdTest";
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $module_title           = $this->module_title;
        $module_name            = $this->module_name;
        $module_path            = $this->module_path;
        $module_icon            = $this->module_icon;
        $module_model           = $this->module_model;

        $module_action          = 'List';

        $list_of_test           = $module_model::orderby('id','desc')->get();

        return view("covidtest::backend.thirdtest.index",compact(
            'module_title',
            'module_name',
            'module_icon',
            'module_action',
            'module_path',
            'list_of_test'
        ));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $module_title   = $this->module_title;
        $module_name    = $this->module_name;
        $module_path    = $this->module_path;
        $module_icon    = $this->module_icon;
        $module_model   = $this->module_model;

        $module_action  = 'Create';

        return view("covidtest::backend.thirdtest.create", compact(
          'module_path',
          'module_title',
          'module_action',
          'module_name',
          'module_icon'
      ));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $module_name     = $this->module_name;
        $module_model    = $this->module_model;
        $module_path     = $this->module_path;

        $module_model::create([
          'name'                    => $request->name,
          'score'                   => $request->score,
          'created_by'              => Auth::user()->id,
          'updated_by'              => 0,
        ]);

        Flash::success("<i class='fas fa-check'></i> New Symptoms and Score Added")->important();

        return redirect()->route("backend.$module_path.index");
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('covidtest::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $module_title   = $this->module_title;
        $module_name    = $this->module_name;
        $module_path    = $this->module_path;
        $module_icon    = $this->module_icon;
        $module_model   = $this->module_model;        

        $list_of_test           = $module_model::findOrFail($id);

        $module_action  = 'Update';

        return view("covidtest::backend.$module_path.edit", compact(
          'list_of_test',
          'module_title',
          'module_action',
          'module_name',
          'module_icon',
          'module_path'
      ));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $module_model   = $this->module_model;
        $module_path    = $this->module_path;

        $list_of_test   = $module_model::findOrFail($id);

        $list_of_test->update([
          'name'                    => $request->name ? $request->name : $list_of_test->name,
          'score'                   => $request->score ? $request->score : $list_of_test->score,
          'updated_by'              => Auth::user()->id,
        ]);

        Flash::success("<i class='fas fa-check'></i>Symptoms and Score Updated")->important();
        return redirect()->route("backend.$module_path.index");
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $module_model = $this->module_model;

        $list_of_test = $module_model::findOrFail($id);

        $list_of_test->delete();

        Flash::success("<i class='fas fa-check'></i> New Symptoms and Score Deleted")->important();
        return back();
    }
}
