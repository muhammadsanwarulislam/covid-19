<?php

namespace Modules\CovidTest\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Flash;

class CovidReportController extends Controller
{
    public function __construct()
    {
        // Page Title
        $this->module_title = 'CovidTest';

        // module name
        $this->module_name = 'firsttest';

        // directory path of the module
        $this->module_path = 'firsttest';

        // module icon
        $this->module_icon = 'fas fa-file-alt';

        // module model name, path
        $this->module_model = "Modules\CovidTest\Entities\FirstTest";
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $module_title           = $this->module_title;
        $module_name            = $this->module_name;
        $module_path            = $this->module_path;
        $module_icon            = $this->module_icon;
        $module_model           = $this->module_model;

        $module_action          = 'List';

        $list_of_test           = $module_model::orderby('id','desc')->get();

        return view("covidtest::backend.report.index",compact(
            'module_title',
            'module_name',
            'module_icon',
            'module_action',
            'module_path',
            'list_of_test'
        ));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('covidtest::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('covidtest::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('covidtest::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
