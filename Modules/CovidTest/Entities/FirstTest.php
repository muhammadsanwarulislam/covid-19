<?php

namespace Modules\CovidTest\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\CovidTest\Entities\SecondTest;

class FirstTest extends Model
{
    protected $fillable = [
    'age',
    'sex',
    'body_temp',
    'firsttest_score',
    'secondtest_item',
    'secondtest_score',
    'thirdtest_item',
    'thirdtest_score',
  ];
  public function firsttestwithsecondtest()
  {

  	return $this->belongsToMany(SecondTest::class);

  }
}
