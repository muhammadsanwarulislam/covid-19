<?php

namespace Modules\CovidTest\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class ThirdTest extends Model
{
    protected $fillable = [
    'name',
    'score',
    'created_by',
    'updated_by',
  ];

  public function users_created_by()
  {
  	return $this->hasOne(User::class,'id','created_by');;
  }
  public function users_updated_by()
  {
  	return $this->hasOne(User::class,'id','updated_by');;
  }
}
