@extends('frontend.layouts.app')

@section('title')
{{app_name()}}
@endsection

@section('content')

<div class="page-header">
    <div class="page-header-image" data-parallax="true" style="background-image: url('{{asset('img/cover-01.jpg')}}');">
    </div>
    <div class="content-center">
        <div class="container">

                <div class="card-body" style="background:white;color:black;">

                    <table id="table" class="display nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>Sl No.</th>
                                <th>Age</th>
                                <th>Sex</th>
                                <th>Temperature</th>
                                <th>Assessment Date</th>
                                <th>Assessment Score</th>
                                <th>COVID-19 Result</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($list_of_test->count()>0)
                            @foreach($list_of_test as $key =>$test)

                            <?php $total_score = $test->firsttest_score + $test->secondtest_score + $test->thirdtest_score ?>
                            <?php $check_sex =$test->sex ?>
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $test->age }}</td>
                                @if($check_sex=='1')
                                    <td>M</td>
                                @else
                                    <td>F</td>
                                @endif
                                <td>{{ $test->body_temp }}</td>
                                <td>{{ date("d M Y",strtotime($test['created_at'])) }}</td>
                                <td>{{ $test->firsttest_score + $test->secondtest_score + $test->thirdtest_score }}</td>
                                @if($total_score < 5)
                                    <td>
                                        <a rel="tooltip" title="নিরাপদ দূরত্ব বজায় রাখুন এবং স্বাস্থ্যবিধি মেনে চলুন , সুস্থ থাকুন ।">
                                        <i class="fas fa-comment-dots"></i>
                                        </a>
                                        Negative
                                    </td>
                                @elseif($total_score >= 5)
                                    <td>
                                        <a rel="tooltip" title="COVID-19 আক্রান্তের সম্ভাব্য সন্দেহজনক কেস বিচ্ছিন্নতা এবং চিকিত্সকের সাথে যোগাযোগের জন্য রোগীকে পরামর্শ দিন এবং অনুসরণ করুন পরামর্শ">
                                        <i class="fas fa-comment-dots"></i>
                                        </a>
                                        Positive
                                    </td>
                                @elseif($total_socre > 7)
                                    <td>
                                        <a rel="tooltip" title="প্রায় নিশ্চিত COVID-19 positive।
                                        রোগীকে বিচ্ছিন্নতা এবং তাত্ক্ষণিক যোগাযোগের জন্য পরামর্শ দিন
                                        এবং পরামর্শ অনুসরণ করুন। উচ্চতর রোগীকে হাসপাতালে ভর্তি হওয়ার পরামর্শ দিন।"><i class="fas fa-comment-dots"></i></a>
                                        Positive
                                    </td>
                                @endif
                            </tr>
                            @endforeach
                            @endif


                        </tbody>
                        
                    </table>
                </div>
                <div class="footer text-center">
                    <a href="{{ route('frontend.step.one') }}" rel="tooltip" title="আসুন আপনার প্রতিরোধ ক্ষমতা পরীক্ষা করার জন্য খেলি" type="submit" class="btn btn-primary btn-round btn-block">Let's Start</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push ('after-styles')
<!-- DataTables Core and Extensions -->
<link rel="stylesheet" href="{{ asset('vendor/datatable/datatables.min.css') }}">

@endpush

@push ('after-scripts')
<!-- DataTables Core and Extensions -->
<script type="text/javascript" src="{{ asset('vendor/datatable/datatables.min.js') }}"></script>

<script>
    $(document).ready(function () {
        $('.table').DataTable();
        var table = $('#table').DataTable( {
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    } );
    });
</script>
@endpush