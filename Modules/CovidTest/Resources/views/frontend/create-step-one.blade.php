@extends('frontend.layouts.app')

@section('title')
{{app_name()}}
@endsection

@section('content')
<div class="page-header">
    <div class="page-header-image" data-parallax="true" style="background-image: url('{{asset('img/cover-01.jpg')}}');">
    </div>
    <div class="content-center">
        <div class="container">
            <form action="{{ route('frontend.step.two') }}" method="POST">
                @csrf

                <div class="card" style="color: black;">
                    <div class="card-header">Step 1: Basic Info</div>

                    <div class="card-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="form-group">
                                <input type="text" value="{{ $covidtest->age ?? '' }}" class="form-control" name="age" placeholder="Provide your age">
                            </div>
                            <div class="form-group">
                                <label class="radio-inline"><input type="radio" name="sex" value="1" {{{ (isset($covidtest->sex) && $covidtest->sex == '1') ? "checked" : "" }}}> Male</label>
                                <label class="radio-inline"><input type="radio" name="sex" value="0" {{{ (isset($covidtest->sex) && $covidtest->sex == '0') ? "checked" : "" }}}> Female</label>
                            </div>
                            <div class="form-group">
                                <input type="text" value="{{ $covidtest->body_temp ?? '' }}" class="form-control" name="body_temp" placeholder="Provide body temperature with celsius">
                            </div>

                        
                    </div>

                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary">Next</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection