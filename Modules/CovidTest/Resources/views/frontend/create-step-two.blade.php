@extends('frontend.layouts.app')

@section('title')
{{app_name()}}
@endsection

@section('content')
<div class="page-header">
    <div class="page-header-image" data-parallax="true" style="background-image: url('{{asset('img/cover-01.jpg')}}');">
    </div>
    <div class="content-center">
        <div class="container">
            <form action="{{ route('frontend.step.four') }}" method="POST">
                @csrf
                <div class="card" style="color: black;">
                    <div class="card-header">Step 2: Health Condition</div>

                    <div class="card-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            @foreach($secondtest as $test)
                            <div class="form-group">
                                <label class="radio-inline">
                                    <input id="secondtest_item" type="radio" name="secondtest_item[{{ $test->id }}]" 
                                    value="{{ $test->name ?? '' }}">
                                    {{$test->name}}
                                </label>
                            </div>
                            @endforeach


                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <a href="{{ route('frontend.step.one') }}" class="btn btn-danger pull-left">Previous</a>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="submit" class="btn btn-primary">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
