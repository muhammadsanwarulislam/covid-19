@extends('backend.layouts.app')

@section('title') {{ __($module_action) }} {{ $module_title }} @stop

@section('breadcrumbs')
<x-backend-breadcrumbs>
    <x-backend-breadcrumb-item type="active" icon='{{ $module_icon }}'>{{ $module_title }}</x-backend-breadcrumb-item>
</x-backend-breadcrumbs>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title mb-0">
                    <i class="{{ $module_icon }}"></i> {{ $module_title }} <small class="text-muted">{{ __($module_action) }}</small>
                </h4>
                <div class="small text-muted">
                    @lang(":module_name Management Dashboard", ['module_name'=>Str::title($module_name)])
                </div>
            </div>
            <!--/.col-->

        </div>
        <!--/.row-->

        <div class="row mt-4">
            <div class="col">
                <table id="datatable" class="table table-bordered table-hover table-responsive-sm">
                    <thead>
                        <tr>
                            <th>Sl No.</th>
                            <th>Age</th>
                            <th>Sex</th>
                            <th>Temperature</th>
                            <th>Assessment Date</th>
                            <th>Assessment Score</th>
                            <th>COVID-19 Result</th>
                        </tr>
                    </thead>

                    <tbody>
                        @if($list_of_test->count()>0)
                        @foreach($list_of_test as $key =>$test)

                        <?php $total_score = $test->firsttest_score + $test->secondtest_score + $test->thirdtest_score ?>
                        <?php $check_sex =$test->sex ?>
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $test->age }}</td>
                            @if($check_sex=='1')
                            <td>M</td>
                            @else
                            <td>F</td>
                            @endif
                            <td>{{ $test->body_temp }}</td>
                            <td>{{ date("d M Y",strtotime($test['created_at'])) }}</td>
                            <td>{{ $test->firsttest_score + $test->secondtest_score + $test->thirdtest_score }}</td>
                            @if($total_score < 5)
                            <td>Negative</td>
                            @elseif($total_score >= 5)
                            <td>Positive</td>
                            @elseif($total_socre > 7)
                            <td>Positive</td>
                            @endif
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
</div>
@stop
@push ('after-styles')
<!-- DataTables Core and Extensions -->
<link rel="stylesheet" href="{{ asset('vendor/datatable/datatables.min.css') }}">

@endpush

@push ('after-scripts')
<!-- DataTables Core and Extensions -->
<script type="text/javascript" src="{{ asset('vendor/datatable/datatables.min.js') }}"></script>

<script>
    $(document).ready(function () {
        $('.table').DataTable();
    });
</script>
@endpush