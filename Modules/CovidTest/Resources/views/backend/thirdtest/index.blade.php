@extends('backend.layouts.app')

@section('title') {{ __($module_action) }} {{ $module_title }} @stop

@section('breadcrumbs')
<x-backend-breadcrumbs>
    <x-backend-breadcrumb-item type="active" icon='{{ $module_icon }}'>{{ $module_title }}</x-backend-breadcrumb-item>
</x-backend-breadcrumbs>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title mb-0">
                    <i class="{{ $module_icon }}"></i> {{ $module_title }} <small class="text-muted">{{ __($module_action) }}</small>
                </h4>
                <div class="small text-muted">
                    @lang(":module_name Management Dashboard", ['module_name'=>Str::title($module_name)])
                </div>
            </div>
            <!--/.col-->
            <div class="col-4">
                <div class="float-right">
                    <x-buttons.create route='{{ route("backend.$module_path.create") }}' title="{{__('Create')}} {{ ucwords(Str::singular($module_name)) }}"/>                   
                </div>
            </div>
            <!--/.col-->
        </div>
        <!--/.row-->

        <div class="row mt-4">
            <div class="col">
                <table id="datatable" class="table table-bordered table-hover table-responsive-sm">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Score
                            </th>
                            <th>
                                Created by
                            </th>
                            <th class="text-left">
                                Action
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        @if($list_of_test->count()>0)
                        @foreach($list_of_test as $key =>$test)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $test->name }}</td>
                            <td>{{ $test->score }}</td>
                            <td>{{ $test->users_created_by ?  $test->users_created_by->name : ' '}}</td>

                            <td>
                                <a href="{{ route("backend.$module_path.edit", $test->id) }}" class="btn btn-success btn-sm" title="Edit">
                                  <span class="fas fa-edit"></span>
                              </a>

                              <a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal{{$test->id}}" title="Delete">
                                  <span class="fas fa-trash"></span>
                              </a>
                          </td>
                        </tr>

                        <!-- Delete Modal -->
                        <div id="deleteModal{{$test->id}}" class="modal fade">
                          <div class="modal-dialog modal-confirm">
                            <div class="modal-content">
                              <div class="modal-header">
                                        <h4 class="modal-title">Are you sure?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                    <div class="modal-body">
                                        <p>Do you really want to delete these records? This process cannot be undone.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <form action="{{ route("backend.$module_path.destroy", $test->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Delete Modal -->

                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
</div>
@stop
