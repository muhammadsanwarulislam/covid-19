<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFirstTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('first_tests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('age')              ->nullable();
            $table->string('sex')               ->nullable();
            $table->string('body_temp')         ->nullable();
            $table->integer('firsttest_score')  ->nullable();
            $table->text('secondtest_item')     ->nullable();
            $table->integer('secondtest_score') ->nullable();
            $table->text('thirdtest_item')      ->nullable();
            $table->integer('thirdtest_score')  ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('first_tests');
    }
}
